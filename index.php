<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alabanzas.mx</title>
    <script src="js/jquery11.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="js/drop.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body onload="get_events();">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Panel de Administracion</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   
                    <li>
                        <a style="cursor:pointer" onclick="get_events()"><i class="fa fa-fw fa-table"></i> Eventos</a>
                    </li>
                    <li>
                        <a href="forms.html"><i class="fa fa-fw fa-music"></i> Canciones</a>
                    </li>
                    
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            


            

        </div>
        <!-- /#page-wrapper -->

        


    </div>

    <div id="modal-box">
        

    </div>

</body>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

    
    <script src="js/jquery.uploadfile.min.js"></script>
    
    <link rel="stylesheet" href="css/uploadfile.min.css"> 
    <link rel="stylesheet" href="css/drop.css"> 
    
   


</body>

</html>

<script>
    function get_events()
    {
        $.get( "inc/get_events.php", function( data ) 
        {

            $('#page-wrapper').html(data);


        });
    }

    function get_modal_add_event()
    {
        $.get( "inc/get_modal_add_event.php", function( data ) 
        {
            $('#modal-box').html(data);
            $("#myModal").modal('show');

        });
    }

    function get_modal_update_event(id_evento)
    {
        $.get( "inc/get_modal_update_event.php?id_evento="+id_evento, function( data ) 
        {
            $('#modal-box').html(data);
            $("#myModal").modal('show');
            prepare_upload('#box-update-modal', id_evento);

        });
    }

    function update_img(id_evento)
    {
         $.get( "inc/update_img.php?id_evento="+id_evento, function( data ) 
        {
            $('#update-img').html(data);

        });
    }

    function prepare_upload(id_box, id_evento)
    {
        var obj = $(id_box);

        obj.on('dragover', function(e)
        {
            e.stopPropagation();
            e.preventDefault();
            $(this).css('border', '2px solid #16a085');

        });

        obj.on('drop', function(e)
        {
            e.stopPropagation();
            e.preventDefault();

            $(this).css('border', '2px solid #bdc3c7');


            var files = e.originalEvent.dataTransfer.files;
            var file = files[0];
            //console.log(file);

            upload(file);
            

        });

        function upload (file) {
            xhr = new XMLHttpRequest();

            name = file.type;
            name_arr = name.split('/');
            extension = name_arr[name_arr.length - 1]

            

            //initiate request  
            xhr.open('post', 'inc/drop.php?ext=' + extension+"&id_evento="+id_evento, true);

            //set headers
            xhr.setRequestHeader('Content-Type', 'multipart/form-data');
            xhr.setRequestHeader('X-File-Name', file.fileName);
            xhr.setRequestHeader('X-File-Size', file.fileSize);
            xhr.setRequestHeader('X-File-Type', file.fileType);

            //send file

            xhr.send(file);
            //alert('final');


        }
    }

    function add_event()
    {

        name_event = $('#name-event-modal').val();
        date_event = $('#date-event-modal').val();
        time_event = $('#time-event-modal').val();
        desc_event = $('#desc-event-modal').val();

        $.get( "inc/add_event.php?name_event="+name_event+"&date_event="+date_event+"&time_event="+time_event+"&desc_event="+desc_event, function( data ) 
        {
            $("#myModal").modal('hide');
            get_events();


        });
    }

</script>

