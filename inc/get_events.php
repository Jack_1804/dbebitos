<?php
	require_once('../../inc/init_db_cat.php');

	$sql_events = 'SELECT * FROM CATALOGO.EVENTOS WHERE US_ID_USUARIO = 1  ORDER BY NOT EVE_ESTADO';
	$result_events = $base_datos_cat->ejecutarQuery($sql_events);

		echo '

		<div style="height:50px">
			<button type="button" class="btn btn-primary pull-right" onclick="get_modal_add_event();"><i class="fa fa-plus-circle"></i></button>
		</div>';


	while($data_events = mysql_fetch_array($result_events))
	{
		$color = "";
		if($data_events['EVE_ESTADO'])
		{ 
			$color = 'green';	
		}
		else
		{
			$color = 'red';
		}

		echo '
		
		 <!-- verde -->

	        <div class="panel panel-'.$color.'">
	            <div class="panel-heading">
	                <div class="row">
	                    <div class="col-xs-2">
	                        <img class="img-responsive" src="../images/event/'.$data_events['EVE_IMG_EVENT'].'" alt="event-image">
	                    </div>
	                    <button style="margin-right: 15px" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <button style="margin-right: 15px" type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="get_modal_update_event('.$data_events['EVE_ID_EVENTO'].')"><i class="fa fa-refresh"></i></button>
	                    <div class="col-xs-9 text-right">

	                        <div class="huge">'.$data_events['EVE_NOMBRE'].'</div>
	                        <div>';

	                        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

							$fecha = $data_events['EVE_FECHA_INI'];
							$fecha = explode(" ", $fecha);
							$hora = $fecha[1];
							$fecha = explode("-", $fecha[0]);
							$dia = $fecha[2];
							$mes = $fecha[1];
							$anio = $fecha[0];
							

							echo $dia." de ".$meses[(int)$mes-1]." del ".$anio;


	                        echo '</div>
	                    </div>
	                </div>
	            </div>
	           

	            <div id="details-content">
	                <div class="panel-footer">
	                    <span class="pull-left"><b>Descripcion: </b>'.$data_events['EVE_DESCRIPCION'].'</span>
	                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                    <div class="clearfix"></div>
	                </div>
	            </div>
	        </div>  

	';
		

	}

	
?>